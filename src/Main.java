import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate date = LocalDate.parse(new Scanner(System.in).next(), formatter);
        if (date.isLeapYear())
            System.out.println(date.getYear() + " - это високосный год");
        else
            System.out.println(date.getYear() + " - не является високосным годом");
    }
}

